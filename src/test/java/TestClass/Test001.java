package TestClass;


import PageAction.LandingPageAction;
import PageAction.signupAction;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestNGListener;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class Test001 extends BaseTest {
    LandingPageAction landingpageaction = new LandingPageAction();
    signupAction signup = new signupAction();


   @Test
    public void Test1() {

        String actual = landingpageaction.geturl(driver);

        String expected = "https://www.demoblaze.com/#";


        Assert.assertEquals(expected, actual);

    }

    @Test
    public void Test2() throws InterruptedException {
        signup.fillform(driver);
        String actual = signup.message(driver);


        Assert.assertEquals(actual, "Sign  successful.", "Username already exists");


    }
}


