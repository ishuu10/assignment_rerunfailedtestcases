package TestNgListner;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class RetryAnalyze implements IRetryAnalyzer {



        int count = 0;
        int maxTry = 2;



    @Override
    public boolean retry(ITestResult iTestResult) {
        if (!iTestResult.isSuccess()) {                      //Check if test not succeed
            if (count < maxTry) {                            //Check if maxtry count is reached
                count++;
                System.out.println("increase the count "+count);//Increase the maxTry count by 1
                iTestResult.setStatus(ITestResult.FAILURE);  //Mark test as failed
                return true;                                 //Tells TestNG to re-run the test
            } else {
                System.out.println(" maxCount reached,test marked as failed "+count);
                iTestResult.setStatus(ITestResult.FAILURE);  //If maxCount reached,test marked as failed
            }
        } else {
            System.out.println("sucess "+count);
            iTestResult.setStatus(ITestResult.SUCCESS);      //If test passes, TestNG marks it as passed
        }
        return false;
    }
}