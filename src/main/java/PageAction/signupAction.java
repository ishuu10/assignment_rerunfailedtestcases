package PageAction;

import PageObjects.signupObjects;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class signupAction  extends signupObjects {
    public void fillform(WebDriver driver) {

        driver.findElement(signup).click();
        Wait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
        wait.until(ExpectedConditions.visibilityOfElementLocated(signuser));

        driver.findElement(signuser).sendKeys("qwerty9566765");
        driver.findElement(signpass).sendKeys("opn");
        driver.findElement(signbutton).click();
    }
    public String message(WebDriver driver) throws InterruptedException {
    Thread.sleep(3000);
       return  driver.switchTo().alert().getText();
    }
}
